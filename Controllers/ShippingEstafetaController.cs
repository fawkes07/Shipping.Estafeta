﻿using Nop.Plugin.Shipping.Estafeta.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Web.Framework.Controllers;
using System.Web.Mvc;

namespace Nop.Plugin.Shipping.Estafeta.Controllers
{
    [AdminAuthorize]
    public class ShippingEstafetaController : BasePluginController
    {
        #region Fields

        private readonly EstafetaSettigns _estafetaSettings;
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;

        #endregion

        #region Ctor

        public ShippingEstafetaController(
            EstafetaSettigns estafetaSettings,
            ILocalizationService localizationService,
            ISettingService settingService)
        {
            this._estafetaSettings = estafetaSettings;
            this._localizationService = localizationService;
            this._settingService = settingService;
        }

        #endregion

        #region Methods

        [ChildActionOnly]
        public ActionResult Configure()
        {
            var model = new EstafetaShippingModel
            {
                WebService_URL = _estafetaSettings.WebService_URL,
                UseSandbox = _estafetaSettings.UseSandbox,
                FreeShippingLine = _estafetaSettings.FreeShippingLine,
                ShippingPrice = _estafetaSettings.ShippingPrice
            };

            return View("~/Plugins/Shipping.Estafeta/Views/ShippingEstafeta/Configure.cshtml", model);
        }

        [HttpPost]
        [ChildActionOnly]
        public ActionResult Configure(EstafetaShippingModel model)
        {
            if (!ModelState.IsValid)
                return Configure();
            
            //save settings
            _estafetaSettings.WebService_URL = model.WebService_URL;
            _estafetaSettings.UseSandbox = model.UseSandbox;
            _estafetaSettings.FreeShippingLine = model.FreeShippingLine;
            _estafetaSettings.ShippingPrice = model.ShippingPrice;

            _settingService.SaveSetting(_estafetaSettings);

            //now clear settings cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

        #endregion

    }
}
