﻿using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Plugin.Shipping.Estafeta.Core;
using CklassDllDTO.NopCommerce;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Services.Shipping;

namespace Nop.Plugin.Shipping.Estafeta.Tasks
{
    public class CheckTrakGuideAvailableTask : ITask
    {
        #region Services

        private readonly ILogger _logger;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IOrderService _orderService;
        private readonly IShipmentService _shipmentService;

        private readonly EstafetaSettigns _estafetaSettigns;

        #endregion

        public CheckTrakGuideAvailableTask(ILogger logger,
                                            IWorkContext workContext,
                                            IStoreContext storeContext,
                                            IOrderService orderService,
                                            IShipmentService shipmentService,
                                            EstafetaSettigns estafetaSettigns)
        {
            this._logger = logger;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._orderService = orderService;
            this._shipmentService = shipmentService;

            this._estafetaSettigns = estafetaSettigns;
        }

        public void Execute()
        {
            #region vars

            List<Pedido> RequestData = new List<Pedido>();
            List<Pedido> response = new List<Pedido>();

            #endregion

            var Orders = _orderService.SearchOrders(
                                storeId: _storeContext.CurrentStore.Id,
                                osIds: new List<int> { (int) OrderStatus.Processing },
                                psIds: new List<int> { (int) PaymentStatus.Paid },
                                ssIds: new List<int> { (int) ShippingStatus.NotYetShipped}
                                );

            foreach(var order in Orders)
            {
                if (!string.IsNullOrWhiteSpace(order.VatNumber))
                    RequestData.Add(new Pedido {
                        PedidoId = order.VatNumber
                    });
            }

            #region Consulta WS

            try
            {
                if (RequestData.Count > 0)
                    response = HttpUtils.Request<List<Pedido>>(_estafetaSettigns.WebService_URL+ "ObtenerGuias", HttpMethod.POST, RequestData);
            }
            catch (Exception ex)
            {
                _logger.Error("Se genero un problema al intentar obtener las guias de rastreo para los pedidos pendientes!", ex);
            }

            #endregion

            var QueryGuias = (from s in response
                              where s.Guia != null && s.Guia.Count > 0
                              select s).ToList();
            
            foreach(var PedidoConGuia in QueryGuias)
            {
                var Order = Orders.First
                    (
                        x => x.VatNumber != null ? 
                        x.VatNumber.Equals(PedidoConGuia.PedidoId, StringComparison.InvariantCultureIgnoreCase) :
                        x.Id.ToString().Equals(PedidoConGuia.PedidoNopId)
                    );

                if (Order != null)
                {
                    foreach (var guia in PedidoConGuia.Guia)
                    {
                        _shipmentService.InsertShipment(new Shipment
                        {
                            OrderId = Order.Id,
                            TrackingNumber = guia.GuiaId,
                            TotalWeight = null,
                            ShippedDateUtc = DateTime.Parse(guia.FechaEnvio),
                            DeliveryDateUtc = null,
                            AdminComment = String.Format("Se envio el {0} ", guia.FechaEnvio),
                            CreatedOnUtc = DateTime.UtcNow,
                        });

                        Order.OrderNotes.Add(new OrderNote
                        {
                            DisplayToCustomer = true,
                            Order = Order,
                            CreatedOnUtc = DateTime.UtcNow,
                            OrderId = Order.Id,
                            Note = String.Format("Su pedido ha salido de nuestro almacen con el siguiente numero de rastreo estafeta: {0}", guia.GuiaId),
                        });
                    }
                    Order.OrderStatus = OrderStatus.Complete;
                    Order.ShippingStatus = ShippingStatus.Shipped;

                    _orderService.UpdateOrder(Order);
                }
            }
        }
    }
}
