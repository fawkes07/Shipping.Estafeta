﻿using System;
using System.Collections.Generic;
using Nop.Services.Shipping.Tracking;
using Nop.Services.Logging;

namespace Nop.Plugin.Shipping.Estafeta
{
    internal class EstafetaShipmentTracker : IShipmentTracker
    {
        private EstafetaSettigns _estafetaSettings;
        private ILogger _logger;

        public EstafetaShipmentTracker(EstafetaSettigns estafetaSettings, ILogger logger)
        {
            this._estafetaSettings = estafetaSettings;
            this._logger = logger;
        }

        public IList<ShipmentStatusEvent> GetShipmentEvents(string trackingNumber)
        {
            // TODO:
            throw new NotImplementedException();
        }

        public string GetUrl(string trackingNumber)
        {
            if (trackingNumber.Length == 22)
                return string.Format("http://rastreo3.estafeta.com/RastreoWebInternet/consultaEnvio.do?dispatch=doRastreoInternet&tipoGuia=REFERENCE&guias={0}&idioma=es", trackingNumber);
            else
                return string.Format("http://rastreo3.estafeta.com/RastreoWebInternet/consultaEnvio.do?dispatch=doRastreoInternet&tipoGuia=ESTAFETA&guias={0}&idioma=es", trackingNumber);
        }

        public bool IsMatch(string trackingNumber)
        {
            return trackingNumber.Length == 22 || trackingNumber.Length == 10;
        }
    }
}