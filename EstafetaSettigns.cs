﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Shipping.Estafeta
{
    public class EstafetaSettigns : ISettings
    {
        /// <summary>
        /// Gets or sets a value to URL where API is hosted
        /// </summary>
        public string WebService_URL { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to developer mode is activate. true - Activate, false - Disable
        /// </summary>
        public bool UseSandbox { get; set; }

        /// <summary>
        /// Gets or sets a value to free shiping line
        /// </summary>
        public decimal FreeShippingLine { get; set; }

        /// <summary>
        /// Gets or sets a value price to Shipping
        /// </summary>
        public decimal ShippingPrice { get; set; }
    }
}
