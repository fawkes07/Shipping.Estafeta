﻿using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tasks;
using Nop.Core.Plugins;
using Nop.Plugin.Shipping.Estafeta.Core;
using Nop.Plugin.Shipping.Estafeta.Models.Request;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Shipping;
using Nop.Services.Shipping.Tracking;
using Nop.Services.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Routing;

namespace Nop.Plugin.Shipping.Estafeta
{
    public class EstafetaComputationMethod : BasePlugin, IShippingRateComputationMethod
    {
        #region Fields

        private readonly EstafetaSettigns _estafetaSettigns;

        private readonly ILogger _logger;
        private readonly IStoreContext _storeContext;
        private readonly ISettingService _settingService;
        private readonly IScheduleTaskService _scheduleTaskService;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;

        #endregion

        #region Ctor

        public EstafetaComputationMethod( 
                        EstafetaSettigns estafetaSettings, 
                        ILogger logger,
                        ISettingService settingService,
                        IStoreContext storeContext,
                        IScheduleTaskService scheduleTaskService,
                        IOrderTotalCalculationService orderTotalCalculationService
                    )
        {
            this._estafetaSettigns = estafetaSettings;
            this._logger = logger;
            this._storeContext = storeContext;
            this._scheduleTaskService = scheduleTaskService;
            this._settingService = settingService;

            _orderTotalCalculationService = orderTotalCalculationService;
        }

        #endregion
        
        #region Properties

        /// <summary>
        /// Gets a shipment tracker
        /// </summary>
        public IShipmentTracker ShipmentTracker
        {
            get
            {
                return new EstafetaShipmentTracker(_estafetaSettigns, _logger);
            }
        }

        /// <summary>
        /// Gets a shipping rate computation method type
        /// </summary>
        public ShippingRateComputationMethodType ShippingRateComputationMethodType
        {
            get
            {
                return ShippingRateComputationMethodType.Offline;
            }
        }

        #endregion

        #region Utils

        public decimal GetRate(Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException("customer");

            var cart = customer.ShoppingCartItems
                                    .Where(x => x.ShoppingCartType == ShoppingCartType.ShoppingCart)
                                    .LimitPerStore(_storeContext.CurrentStore.Id)
                                    .ToList();

            decimal subtotal = decimal.Zero;

            foreach (var sci in cart)
            {
                subtotal += (sci.Quantity * sci.Product.Price);
            }

            Flete responser = new Flete();
            if (subtotal < _estafetaSettigns.FreeShippingLine)
            {
                try
                {
                    responser = HttpUtils.Request<Flete>(_estafetaSettigns.WebService_URL + "ObtenerGastosEnvio/" + customer.Id, HttpMethod.GET, null);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message, ex, customer);
                }

                return responser.Success ? responser.Costo : _estafetaSettigns.ShippingPrice;
            }
            else
                return decimal.Zero;
            
        }

        #endregion

        #region Methods

        public override void Install()
        {
            //settings
            var settings = new EstafetaSettigns
            {
#if DEBUG
                FreeShippingLine = 0,
                ShippingPrice = 0,
                WebService_URL = "https://pedidos.cklass.net/AppDebug/ServicesMov.svc/",
                UseSandbox = true
#else
                FreeShippingLine = 0,
                ShippingPrice = 0,
                WebService_URL = "https://pedidos.cklass.net/App/ServicesMov.svc/",
                UseSandbox = false
#endif
            };

            _settingService.SaveSetting(settings);

            // Locales
#region  locales

            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Estafeta.Fields.WebService_URL", "URL WebService");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Estafeta.Fields.WebService_URL.Hint", "URL donde se aloja el WebService");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Estafeta.Fields.UseSandbox", "Use Sandbox");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Estafeta.Fields.UseSandbox.Hint", "Check to enable Sandbox (testing environment).");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Estafeta.Fields.FreeShippingLine", "Free Shipping line");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Estafeta.Fields.FreeShippingLine.Hint", "A partir de esta cantidad (total del pedido) se aplica envio gratis");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Estafeta.Fields.ShippingPrice", "Tarifa de envio predeterminada");
            this.AddOrUpdatePluginLocaleResource("Plugins.Shipping.Estafeta.Fields.ShippingPrice.Hint", "Tarifa que aplica para todos los envios mayores al Free Shipping line siempre que el WS falle");

#endregion

            // Insert Task
            _scheduleTaskService.InsertTask(new ScheduleTask
            {
                Enabled = true,
                Name = "Actualizacion de guias de rastreo ESTAFETA",
                Seconds = (60 * 60) * 2,                //60 seg x 60 min = 3600 seg in 1 hour
                StopOnError = false,
                Type = "Nop.Plugin.Shipping.Estafeta.Tasks.CheckTrakGuideAvailableTask, Nop.Plugin.Shipping.Estafeta"
            });

            base.Install();
        }

        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<EstafetaSettigns>();

            //locales
            this.DeletePluginLocaleResource("Plugins.Shipping.Estafeta.Fields.WebService_URL");
            this.DeletePluginLocaleResource("Plugins.Shipping.Estafeta.Fields.WebService_URL.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.CanadaPost.Fields.CustomerNumber");
            this.DeletePluginLocaleResource("Plugins.Shipping.CanadaPost.Fields.CustomerNumber.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.CanadaPost.Fields.UseSandbox");
            this.DeletePluginLocaleResource("Plugins.Shipping.CanadaPost.Fields.UseSandbox.Hint");
            this.DeletePluginLocaleResource("Plugins.Shipping.Estafeta.Fields.ShippingPrice");
            this.DeletePluginLocaleResource("Plugins.Shipping.Estafeta.Fields.ShippingPrice.Hint");


            ScheduleTask task = _scheduleTaskService
                                    .GetTaskByType("Nop.Plugin.Shipping.Estafeta.Tasks.CheckTrakGuideAvailableTask, Nop.Plugin.Shipping.Estafeta");

            if (task != null)
            {
                _scheduleTaskService.DeleteTask(task);
            }

            base.Uninstall();
        }

        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "ShippingEstafeta";
            routeValues = new RouteValueDictionary
                            {
                                { "Namespaces", "Nop.Plugin.Shipping.Estafeta.Controllers" },
                                { "area", null }
                            };
        }

        /// <summary>
        /// Gets fixed shipping rate (if shipping rate computation method allows it and the rate can be calculated before checkout).
        /// </summary>
        /// <param name="getShippingOptionRequest">A request for getting shipping options</param>
        /// <returns>Fixed shipping rate; or null in case there's no fixed shipping rate</returns>
        public decimal? GetFixedRate(GetShippingOptionRequest getShippingOptionRequest)
        {
            if (getShippingOptionRequest == null)
            {
                _logger.Error("Estafeta > GetFixedRate > getShippingOptionRequest esta nulo");
                throw new ArgumentNullException("getShippingOptionRequest");
            }

            return GetRate(getShippingOptionRequest.Customer);
        }

        /// <summary>
        ///  Gets available shipping options
        /// </summary>
        /// <param name="getShippingOptionRequest">A request for getting shipping options</param>
        /// <returns>Represents a response of getting shipping rate options</returns>
        public GetShippingOptionResponse GetShippingOptions(GetShippingOptionRequest getShippingOptionRequest)
        {
            if (getShippingOptionRequest == null)
            {
                _logger.Error("Estafeta > GetShippingOptions > getShippingOptionRequest esta nulo");
                throw new ArgumentNullException("getShippingOptionRequest");
            }

            if (getShippingOptionRequest.Items == null)
                return new GetShippingOptionResponse { Errors = new List<string> { "No existen items para enviar" } };

            if (getShippingOptionRequest.ShippingAddress == null)
                return new GetShippingOptionResponse { Errors = new List<string> { "Direccion de envio no fue ingresada" } };

            if (getShippingOptionRequest.ShippingAddress.Country == null)
                return new GetShippingOptionResponse { Errors = new List<string> { "Lo sentimos, no podemos hacer envios a su pais" } };

            if (string.IsNullOrEmpty(getShippingOptionRequest.ShippingAddress.ZipPostalCode))
                return new GetShippingOptionResponse { Errors = new List<string> { "No a ingresado su codigo postal" } };
            
            if (getShippingOptionRequest.Items == null || !getShippingOptionRequest.Items.Any())
                return new GetShippingOptionResponse { Errors = new List<string> { "No existen item para enviar" } };

            var opt = new ShippingOption
                            {
                                Name = "Estafeta terrestre",
                                Rate = GetRate(getShippingOptionRequest.Customer),
                                Description = "Terrestre de 5 a 10 dias"
                            };

            return new GetShippingOptionResponse { ShippingOptions = new List<ShippingOption> { opt } };
        }

#endregion
    }
}
