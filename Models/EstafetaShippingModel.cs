﻿using Nop.Web.Framework;

namespace Nop.Plugin.Shipping.Estafeta.Models
{
    public class EstafetaShippingModel
    {
        [NopResourceDisplayName("Plugins.Shipping.Estafeta.Fields.WebService_URL")]
        public string WebService_URL { get; set; }

        [NopResourceDisplayName("Plugins.Shipping.Estafeta.Fields.UseSandbox")]
        public bool UseSandbox { get; set; }

        [NopResourceDisplayName("Plugins.Shipping.Estafeta.Fields.FreeShippingLine")]
        public decimal FreeShippingLine { get; set; }

        [NopResourceDisplayName("Plugins.Shipping.Estafeta.Fields.ShippingPrice")]
        public decimal ShippingPrice { get; set; }
    }
}
