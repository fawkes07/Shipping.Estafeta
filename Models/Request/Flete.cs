﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nop.Plugin.Shipping.Estafeta.Models.Request
{
    public class Flete
    {
        public string PedidoId { get; set; }

        public decimal Costo { get; set; }

        public bool Success { get; set; }

        public string MessageResult { get; set; }
    }
}
